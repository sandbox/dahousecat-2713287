
-- SUMMARY --

Allows using theme_table to create tables where the header is down the left hand
side of the table as opposed to along the top.

For a full description of the module, visit the project page:
  http://drupal.org/project/vertical_tables

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/vertical_tables


-- REQUIREMENTS --

Drupal 7.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- KNOWN LIMITATIONS --

- No support for colgroups
- No sticky headers
- No sorting


-- EXAMPLE --

$header = array('Apples', 'Peaches', 'Pears', 'Bananas');
$columns[] = array('Lorem', 'ipsum', 'dolor', 'sit');
$columns[] = array('amet', 'consectetur', 'adipiscing', 'elit');
$variables = array(
  'vertical' => TRUE,
  'header' => $header,
  'columns' => $columns,
  'empty' => 'No data found',
);
$table = theme('table', $variables);
